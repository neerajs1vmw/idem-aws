import copy
import uuid
from collections import ChainMap

import pytest


@pytest.mark.localstack(pro=False)
@pytest.mark.asyncio
async def test_ami(hub, ctx, aws_snapshot):
    ami_temp_name = "idem-test-ami-" + str(uuid.uuid4())
    root_device_name = "/dev/sda1"

    snapshot_id = aws_snapshot
    block_device_mappings = [
        {
            "DeviceName": "/dev/sda1",
            "Ebs": {
                "DeleteOnTermination": False,
                "SnapshotId": f"{snapshot_id}",
                "VolumeSize": 8,
                "VolumeType": "gp2",
            },
        }
    ]

    description = "Amazon Linux AMI 2018.03.0.20181129 x86_64 Minimal HVM ebs"
    tags = {"idem-test-ami-key": "idem-test-ami-value"}

    new_tags = {"idem-test-ami-new-key": "idem-test-ami-new-value"}

    # Create ami with test flag
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True

    ret = await hub.states.aws.ec2.ami.present(
        test_ctx,
        name=ami_temp_name,
        block_device_mappings=block_device_mappings,
        root_device_name=root_device_name,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert f"Would create aws.ec2.ami '{ami_temp_name}'" in ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    created_ami_name = resource.get("name")

    assert block_device_mappings == ret["new_state"]["block_device_mappings"]
    assert root_device_name == ret["new_state"]["root_device_name"]
    assert ret["result"], ret["comment"]
    assert created_ami_name == ami_temp_name
    assert tags == ret["new_state"]["tags"]
    assert "/dev/sda1" == resource.get("block_device_mappings")[0].get("DeviceName")

    # Create ami in real
    ret = await hub.states.aws.ec2.ami.present(
        ctx,
        name=ami_temp_name,
        block_device_mappings=block_device_mappings,
        root_device_name=root_device_name,
        tags=tags,
    )

    assert ret["result"], ret["comment"]
    assert f"Created aws.ec2.ami '{ami_temp_name}'" in ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    created_ami_name = resource.get("name")
    resource_id = resource.get("resource_id")

    assert root_device_name == ret["new_state"]["root_device_name"]
    assert ret["result"], ret["comment"]
    assert created_ami_name == ami_temp_name
    assert tags == ret["new_state"]["tags"]
    assert "/dev/sda1" == ret["new_state"].get("block_device_mappings")[0].get(
        "DeviceName"
    )

    # verify that created ami is present (describe)
    describe_ret = await hub.states.aws.ec2.ami.describe(ctx)
    assert resource_id in describe_ret
    assert "aws.ec2.ami.present" in describe_ret.get(resource_id)

    described_resource = describe_ret.get(resource_id).get("aws.ec2.ami.present")
    described_resource_map = dict(ChainMap(*described_resource))
    assert root_device_name == described_resource_map.get("root_device_name")
    assert ami_temp_name == described_resource_map.get("name")
    assert tags == described_resource_map.get("tags")
    assert "/dev/sda1" == described_resource_map.get("block_device_mappings")[0].get(
        "DeviceName"
    )

    # Create ami again with same resource_id (no change in state)
    ret = await hub.states.aws.ec2.ami.present(
        ctx,
        name=ami_temp_name,
        block_device_mappings=block_device_mappings,
        root_device_name=root_device_name,
        resource_id=resource_id,
        tags=tags,
    )
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("old_state")
    assert root_device_name == resource.get("root_device_name")
    assert ami_temp_name == resource.get("name")
    assert tags == resource.get("tags")
    assert "/dev/sda1" == resource.get("block_device_mappings")[0].get("DeviceName")

    resource = ret.get("new_state")
    assert root_device_name == resource.get("root_device_name")
    assert ami_temp_name == resource.get("name")
    assert tags == resource.get("tags")
    assert "/dev/sda1" == resource.get("block_device_mappings")[0].get("DeviceName")

    # AMI Update, present state with same resource_id and with different description and tag

    # test flag
    test_ctx["test"] = True
    ret = await hub.states.aws.ec2.ami.present(
        test_ctx,
        name=ami_temp_name,
        block_device_mappings=block_device_mappings,
        root_device_name=root_device_name,
        resource_id=resource_id,
        description=description,
        tags=new_tags,
    )
    assert ret["result"], ret["comment"]
    assert f"Would update aws.ec2.ami '{ami_temp_name}'" in ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    resource_id = resource.get("resource_id")

    assert ret["result"], ret["comment"]
    assert root_device_name == resource.get("root_device_name")
    assert ami_temp_name == resource.get("name")
    assert new_tags == resource.get("tags")
    assert description == resource.get("description")
    assert "/dev/sda1" == resource.get("block_device_mappings")[0].get("DeviceName")

    # With real context
    ret = await hub.states.aws.ec2.ami.present(
        ctx,
        name=ami_temp_name,
        block_device_mappings=block_device_mappings,
        root_device_name=root_device_name,
        resource_id=resource_id,
        description=description,
        tags=new_tags,
    )

    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    assert f"Updated aws.ec2.ami '{ami_temp_name}'" in ret["comment"]
    resource = ret.get("old_state")
    resource_id = resource.get("resource_id")
    assert root_device_name == resource.get("root_device_name")
    assert ami_temp_name == resource.get("name")
    assert tags == resource.get("tags")
    assert "/dev/sda1" == resource.get("block_device_mappings")[0].get("DeviceName")

    resource = ret.get("new_state")
    resource_id = resource.get("resource_id")
    assert root_device_name == resource.get("root_device_name")
    assert ami_temp_name == resource.get("name")
    assert new_tags == resource.get("tags")
    assert "/dev/sda1" == resource.get("block_device_mappings")[0].get("DeviceName")
    assert ret["result"], ret["comment"]

    # With real aws, modify_image_attribute actually modify the description and new_state(describe_images on updated
    # image) contains modified value. With localstack modify_image_attribute api returns success status but actual
    # description is not changed in localstack, describe_images still returns old description
    if not hub.tool.utils.is_running_localstack(ctx):
        assert description == resource.get("description")

    # Delete AMI with test flag
    ret = await hub.states.aws.ec2.ami.absent(
        test_ctx, name=ami_temp_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    assert f"Would delete aws.ec2.ami '{ami_temp_name}'" in ret["comment"]
    resource = ret.get("old_state")
    assert root_device_name == resource.get("root_device_name")
    assert ami_temp_name == resource.get("name")
    assert new_tags == resource.get("tags")
    assert "/dev/sda1" == resource.get("block_device_mappings")[0].get("DeviceName")

    # Delete AMI in real
    ret = await hub.states.aws.ec2.ami.absent(
        ctx, name=ami_temp_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    assert f"Deleted aws.ec2.ami '{ami_temp_name}'" in ret["comment"]
    resource = ret.get("old_state")
    resource_id = resource.get("resource_id")
    assert root_device_name == resource.get("root_device_name")
    assert ami_temp_name == resource.get("name")
    assert new_tags == resource.get("tags")
    assert "/dev/sda1" == resource.get("block_device_mappings")[0].get("DeviceName")

    # Deleting the same AMI again (deleted state) will not invoke delete on AWS side.
    # real aws returns empty list of images for describe_images when we pass non-existence resource_id,
    # localstack returns error
    if not hub.tool.utils.is_running_localstack(ctx):
        ret = await hub.states.aws.ec2.ami.absent(
            ctx, name=ami_temp_name, resource_id=resource_id
        )
        assert ret["result"], ret["comment"]
        assert not ret.get("old_state") and not ret.get("new_state")
        assert f"aws.ec2.ami '{ami_temp_name}' already absent" in ret["comment"]
