"""
Test simple creation and deletion of a network_interface
"""
import pprint

import pytest


# Parametrization options for running each test with --test first and then without --test
@pytest.mark.localstack(pro=True)
@pytest.mark.dependency(name="present")
@pytest.mark.asyncio
async def test_present(hub, ctx, network_interface_name, aws_ec2_subnet, __test):
    """
    Create a vanilla network_interface without using the network_interface fixture
    """
    ret = await hub.states.aws.ec2.network_interface.present(
        ctx,
        name=network_interface_name,
        client_token=network_interface_name,
        subnet_id=aws_ec2_subnet["SubnetId"],
        tags={"Name": network_interface_name},
    )

    assert ret["result"], pprint.pformat(ret["comment"])
    assert ret["new_state"], ret["comment"]

    if ctx.test:
        return

    # Verify that we can perform a successful "get" and that the id matches the present new_state
    get = await hub.exec.aws.ec2.network_interface.get(
        ctx,
        filters=[
            {
                "Name": "network-interface-id",
                "Values": [ret["new_state"]["resource_id"]],
            }
        ],
    )
    assert get.result, get.comment
    assert get.ret, get.comment
    assert get.ret["resource_id"] == ret["new_state"]["resource_id"]


@pytest.mark.localstack(pro=True)
@pytest.mark.dependency(depends=["present"])
@pytest.mark.asyncio
async def test_get(hub, ctx, network_interface_name):
    """
    Verify that "get" is successful after an network_interfaces has been created
    """
    get = await hub.exec.aws.ec2.network_interface.get(
        ctx, filters=[{"Name": "tag:Name", "Values": [network_interface_name]}]
    )
    assert get.result, get.comment
    assert get.ret, get.comment

    ret = await hub.exec.aws.ec2.network_interface.get(
        ctx, resource_id=get.ret.resource_id
    )
    assert ret.result, ret.comment
    assert ret.ret, ret.comment

    # Verify that the network_interface id matches for both
    assert ret.ret["resource_id"] == get.ret["resource_id"]


@pytest.mark.localstack(pro=True)
@pytest.mark.dependency(depends=["present"])
@pytest.mark.asyncio
async def test_list(hub, ctx, network_interface_name):
    """
    Verify that "list" is successful after an network_interfaces has been created
    """
    get = await hub.exec.aws.ec2.network_interface.get(
        ctx, filters=[{"Name": "tag:Name", "Values": [network_interface_name]}]
    )

    ret = await hub.exec.aws.ec2.network_interface.list(
        ctx, filters=[{"Name": "network-interface-id", "Values": [get.ret.resource_id]}]
    )
    assert ret.result, ret.comment
    assert ret.ret, ret.comment
    # Verify that the created network_interface is in the list
    assert ret.ret[0]["resource_id"] == get.ret.resource_id


@pytest.mark.localstack(False)
@pytest.mark.asyncio
async def test_fixture(hub, ctx, aws_ec2_network_interface, network_interface_name):
    """
    Use the network_interface fixture and verify that it is functional.
    Nothing new should be created by the fixture in this test since both the fixture
    and the network_interface present state tests use the "network_interface_name" module level fixture
    """
    get = await hub.exec.aws.ec2.network_interface.list(
        ctx, filters=[{"Name": "tag:Name", "Values": [network_interface_name]}]
    )
    # Verify that the fixture resulted in a usable network_interface
    assert aws_ec2_network_interface["resource_id"] == get.ret.resource_id


@pytest.mark.localstack(pro=True)
@pytest.mark.asyncio
async def test_describe(hub, ctx, network_interface_name, __test):
    """
    Describe all network_interfaces and run the "present" state the described network_interface created for this module.
    No changes should be made and present/search/describe should have equivalent parameters.
    """
    get = await hub.exec.aws.ec2.network_interface.get(
        ctx, filters=[{"Name": "tag:Name", "Values": [network_interface_name]}]
    )

    # Describe all network_interfaces
    ret = await hub.states.aws.ec2.network_interface.describe(ctx)
    assert get.ret.resource_id in ret

    # Run the present state for our resource created by describe
    network_interface_kwargs = {}
    for pair in ret[get.ret.resource_id]["aws.ec2.network_interface.present"]:
        network_interface_kwargs.update(pair)

    # Run the present state on the result of "describe, no changes should be made
    network_interface_ret = await hub.states.aws.ec2.network_interface.present(
        ctx, **network_interface_kwargs
    )

    assert network_interface_ret["result"], network_interface_ret["comment"]

    # No changes should have been made!
    # We just created this state from describe
    assert network_interface_ret["old_state"] == network_interface_ret["new_state"]
    assert not network_interface_ret["changes"]


@pytest.mark.localstack(pro=True)
@pytest.mark.asyncio
async def test_absent(hub, ctx, network_interface_name, __test):
    """
    Destroy the network_interface created by the present state
    """
    get = await hub.exec.aws.ec2.network_interface.get(
        ctx, filters=[{"Name": "tag:Name", "Values": [network_interface_name]}]
    )

    if get.result is True and get.ret is not None:
        ret = await hub.states.aws.ec2.network_interface.absent(
            ctx, name=network_interface_name, resource_id=get.ret.resource_id
        )
    else:
        ret = await hub.states.aws.ec2.network_interface.absent(
            ctx, name=network_interface_name, resource_id=None
        )

    assert ret["result"], ret["comment"]
    assert not ret["new_state"]
