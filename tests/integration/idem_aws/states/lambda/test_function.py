import copy
import time
import uuid
from collections import ChainMap

import pytest


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
async def test_function(hub, ctx, aws_iam_role_2, zip_file, aws_kms_key):
    # Create lambda function
    function_name = "idem-test-lambda-function" + str(uuid.uuid4())
    tags = {"Key": "Name", "Value": function_name}
    description = "Process image objects from Amazon S3."
    code = {"ZipFile": zip_file}
    tracing_config = {"Mode": "Active"}

    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True

    # Idem state --test
    response = await hub.states.aws.lambda_aws.function.present(
        test_ctx,
        code=code,
        description=description,
        name=function_name,
        handler="code.main",
        kms_key_arn=aws_kms_key.get("arn"),
        memory_size=256,
        publish=True,
        role=aws_iam_role_2["arn"],
        runtime="python3.9",
        tags=tags,
        function_timeout=15,
        tracing_config=tracing_config,
        architectures=["x86_64"],
    )

    assert response["result"], response["comment"]
    assert not response.get("old_state") and response.get("new_state")
    resource = response.get("new_state")
    assert function_name == resource.get("name")
    assert resource.get("handler") == "code.main"
    assert resource.get("role") == aws_iam_role_2["arn"]
    assert resource.get("runtime") == "python3.9"
    assert resource.get("function_timeout") == 15
    assert resource.get("tracing_config") == tracing_config
    assert resource.get("tags")
    assert resource.get("tags").get("Key") == tags.get("Key")
    assert resource.get("tags").get("Value") == tags.get("Value")
    assert resource.get("description") == description
    assert resource.get("architectures")[0] == "x86_64"
    assert resource.get("kms_key_arn") == aws_kms_key.get("arn")

    # this is to ensure the created role 'aws_iam_role_2' is fully functional and is ready to be
    # assumed by Lambda service. Since role doesn't have a status field, tests are repeatedly failing
    # with exception: 'InvalidParameterValueException: An error occurred (InvalidParameterValueException) when calling
    #                  the CreateFunction operation: The role defined for the function cannot be assumed by Lambda.',)
    if not hub.tool.utils.is_running_localstack(ctx):
        time.sleep(45)

    # create lambda function.
    response = await hub.states.aws.lambda_aws.function.present(
        ctx,
        code=code,
        description=description,
        name=function_name,
        handler="code.main",
        memory_size=256,
        publish=True,
        role=aws_iam_role_2["arn"],
        runtime="python3.9",
        tags=tags,
        function_timeout=15,
        tracing_config=tracing_config,
        architectures=["x86_64"],
    )

    assert response["result"], response["comment"]
    assert not response.get("old_state") and response.get("new_state")
    resource = response.get("new_state")
    assert function_name == resource.get("name")
    assert "code.main" == resource.get("handler")
    assert aws_iam_role_2["arn"] == resource.get("role")
    assert "python3.9" == resource.get("runtime")
    assert 15 == resource.get("function_timeout")
    assert tracing_config == resource.get("tracing_config")
    assert description == resource.get("description")
    assert "x86_64" == resource.get("architectures")[0]
    assert resource.get("tags")
    assert resource.get("tags").get("Key") == tags.get("Key")
    assert resource.get("tags").get("Value") == tags.get("Value")
    resource_id = resource.get("name")

    description = "Process image objects from Amazon S3. Updated."
    tracing_config = {
        "Mode": "PassThrough",
    }
    tags = {"Name": function_name, "new_tag": "new_value"}

    # Idem state --test
    update_response = await hub.states.aws.lambda_aws.function.present(
        test_ctx,
        code=code,
        resource_id=resource_id,
        description=description,
        role=aws_iam_role_2["arn"],
        name=function_name,
        tags=tags,
        tracing_config=tracing_config,
        architectures=["arm64"],
    )

    assert update_response["result"], update_response["comment"]
    assert update_response.get("old_state") and update_response.get("new_state")
    resource = update_response.get("new_state")
    assert function_name == resource.get("name")
    assert "code.main" == resource.get("handler")
    assert aws_iam_role_2["arn"] == resource.get("role")
    assert "python3.9" == resource.get("runtime")
    assert 15 == resource.get("function_timeout")
    assert tracing_config == resource.get("tracing_config")
    assert description == resource.get("description")
    assert "arm64" == resource.get("architectures")[0]
    assert resource.get("tags")
    assert list(tags.keys()) == resource.get("tags")

    # Update Function.
    update_response = await hub.states.aws.lambda_aws.function.present(
        ctx,
        code=code,
        resource_id=resource_id,
        description=description,
        name=function_name,
        role=aws_iam_role_2["arn"],
        tags=tags,
        tracing_config=tracing_config,
        architectures=["arm64"],
    )

    assert update_response["result"], update_response["comment"]
    assert update_response.get("old_state") and update_response.get("new_state")
    resource = update_response.get("new_state")
    assert function_name == resource.get("name")
    assert "code.main" == resource.get("handler")
    assert aws_iam_role_2["arn"] == resource.get("role")
    assert "python3.9" == resource.get("runtime")
    assert 15 == resource.get("function_timeout")
    assert tracing_config == resource.get("tracing_config")
    assert description == resource.get("description")
    assert "arm64" == resource.get("architectures")[0]
    assert resource.get("tags")
    assert tags == resource.get("tags")

    # Idem state --test. update lambda function. This time with no tags in update. It should return old tags only.
    update_response = await hub.states.aws.lambda_aws.function.present(
        test_ctx,
        code=code,
        role=aws_iam_role_2["arn"],
        name=function_name,
        resource_id=resource_id,
    )
    assert update_response["result"], update_response["comment"]
    assert update_response.get("old_state") and update_response.get("new_state")
    new_resource = update_response.get("new_state")
    old_resource = update_response.get("old_state")
    assert new_resource.get("tags") == old_resource.get("tags")
    assert old_resource.get("tags").get("Name") == new_resource.get("tags").get("Name")
    assert old_resource.get("tags").get("new_tag") == new_resource.get("tags").get(
        "new_tag"
    )

    # update lambda function. This time with no tags in update. It should return old tags only.
    update_response = await hub.states.aws.lambda_aws.function.present(
        ctx,
        code=code,
        role=aws_iam_role_2["arn"],
        name=function_name,
        resource_id=resource_id,
    )
    assert update_response["result"], update_response["comment"]
    assert update_response.get("old_state") and update_response.get("new_state")
    new_resource = update_response.get("new_state")
    old_resource = update_response.get("old_state")
    assert new_resource.get("tags") == old_resource.get("tags")
    assert old_resource.get("tags").get("Name") == new_resource.get("tags").get("Name")
    assert old_resource.get("tags").get("new_tag") == new_resource.get("tags").get(
        "new_tag"
    )

    # Describe dynamodb_table
    describe_response = await hub.states.aws.lambda_aws.function.describe(ctx)
    assert describe_response[function_name]
    assert describe_response.get(function_name) and describe_response.get(
        function_name
    ).get("aws.lambda.function.present")
    described_resource = describe_response.get(function_name).get(
        "aws.lambda.function.present"
    )
    resource = dict(ChainMap(*described_resource))
    assert function_name == resource.get("name")
    assert "code.main" == resource.get("handler")
    assert aws_iam_role_2["arn"] == resource.get("role")
    assert "python3.9" == resource.get("runtime")
    assert 15 == resource.get("function_timeout")
    assert tracing_config == resource.get("tracing_config")
    assert description == resource.get("description")
    assert "arm64" == resource.get("architectures")[0]
    assert resource.get("tags")
    assert (len(resource.get("tags")) == 2) and all(
        tag in resource.get("tags") for tag in tags
    )

    # Idem state --test. Delete dynamodb_table
    delete_response = await hub.states.aws.lambda_aws.function.absent(
        test_ctx,
        name=function_name,
        resource_id=resource_id,
    )
    assert delete_response["result"], delete_response["comment"]
    assert delete_response.get("old_state") and not delete_response.get("new_state")

    # Delete dynamodb_table
    delete_response = await hub.states.aws.lambda_aws.function.absent(
        ctx,
        name=function_name,
        resource_id=resource_id,
    )
    assert delete_response["result"], delete_response["comment"]
    assert delete_response.get("old_state") and not delete_response.get("new_state")
    assert delete_response.get("old_state")
    resource = delete_response.get("old_state")
    assert function_name == resource.get("name")
    assert "code.main" == resource.get("handler")
    assert aws_iam_role_2["arn"] == resource.get("role")
    assert "python3.9" == resource.get("runtime")
    assert 15 == resource.get("function_timeout")
    assert tracing_config == resource.get("tracing_config")
    assert resource.get("tags")
    assert tags.get("Key") == resource.get("tags").get("Key")
    assert tags.get("Value") == resource.get("tags").get("Value")
    assert description == resource.get("description")
    assert "arm64" == resource.get("architectures")[0]

    # Trying to delete an already deleted resource. It should say resource already got deleted.
    ret = await hub.states.aws.lambda_aws.function.absent(
        ctx,
        name=function_name,
        resource_id=resource_id,
    )
    assert f"aws.lambda.function '{function_name}' already absent" in ret["comment"]
