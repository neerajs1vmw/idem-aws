import time

import pytest


@pytest.mark.asyncio
async def test_get_not_existing_resource_id(hub, ctx):
    instance_get_name = "idem-test-exec-get-instance-" + str(int(time.time()))
    ret = await hub.exec.aws.ec2.instance.get(
        ctx, name=instance_get_name, resource_id="i-0102ced1c6fd20909"
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"] is None
    assert f"Get aws.ec2.instance '{instance_get_name}' result is empty" in str(
        ret["comment"]
    )
