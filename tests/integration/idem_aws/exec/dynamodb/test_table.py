import time

import pytest

# Test test_get() is in states/dynamodb/test_table.py


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
async def test_get_invalid_resource_id(hub, ctx):
    table_name = "idem-test-exec-get-table-" + str(int(time.time()))
    ret = await hub.exec.aws.dynamodb.table.get(
        ctx,
        name=table_name,
        resource_id="fake-id",
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"] is None
    assert (
        hub.tool.aws.comment_utils.get_empty_comment(
            "aws.dynamodb.table",
            table_name,
        )
        in ret["comment"]
    )
