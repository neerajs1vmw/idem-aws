import copy

import pytest

PARAMETRIZE = dict(argnames="__test", argvalues=[True, False], ids=["--test", "run"])
resource_type = "aws.guardduty.organization_configuration"


@pytest.mark.asyncio
@pytest.mark.localstack(
    False,
    "Skipping test in localstack as guardduty is not supported in localstack.",
)
@pytest.mark.parametrize(**PARAMETRIZE)
async def test_update_organization_configuration_without_data_sources(
    hub, ctx, __test, aws_guardduty_detector, aws_guardduty_organization_master_account
):
    if hub.tool.utils.is_running_localstack(ctx):
        return
    ctx["test"] = __test
    detector_id = aws_guardduty_detector.get("resource_id")
    auto_enable = True

    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await hub.exec.aws.guardduty.organization_configuration.update(
        ctx,
        resource_id=detector_id,
        auto_enable=auto_enable,
    )
    assert ret["result"], ret["comment"]
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_update_comment(
                resource_type=resource_type,
                name=detector_id,
            )[0]
            in ret["comment"]
        )
    else:
        assert (
            hub.tool.aws.comment_utils.update_comment(
                resource_type=resource_type,
                name=detector_id,
            )
            in ret["comment"]
        )


@pytest.mark.asyncio
@pytest.mark.localstack(
    False,
    "Skipping test in localstack as guardduty is not supported in localstack.",
)
@pytest.mark.parametrize(**PARAMETRIZE)
async def test_organization_configuration_update_data_sources(
    hub, ctx, __test, aws_guardduty_detector, aws_guardduty_organization_master_account
):
    if hub.tool.utils.is_running_localstack(ctx):
        return
    ctx["test"] = __test
    detector_id = aws_guardduty_detector.get("resource_id")
    auto_enable = True
    data_sources = {
        "S3Logs": {"AutoEnable": True},
        "Kubernetes": {"AuditLogs": {"AutoEnable": True}},
        "MalwareProtection": {
            "ScanEc2InstanceWithFindings": {"EbsVolumes": {"AutoEnable": False}}
        },
    }
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await hub.exec.aws.guardduty.organization_configuration.update(
        ctx, resource_id=detector_id, auto_enable=auto_enable, data_sources=data_sources
    )
    assert ret["result"], ret["comment"]
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_update_comment(
                resource_type=resource_type,
                name=detector_id,
            )[0]
            in ret["comment"]
        )
    else:
        assert (
            hub.tool.aws.comment_utils.update_comment(
                resource_type=resource_type,
                name=detector_id,
            )
            in ret["comment"]
        )


@pytest.mark.asyncio
@pytest.mark.localstack(
    False,
    "Skipping test in localstack as guardduty is not supported in localstack.",
)
async def test_organization_configuration_no_update(
    hub, ctx, aws_guardduty_detector, aws_guardduty_organization_master_account
):
    if hub.tool.utils.is_running_localstack(ctx):
        return
    detector_id = aws_guardduty_detector.get("resource_id")
    data_sources = {
        "S3Logs": {"AutoEnable": True},
        "Kubernetes": {"AuditLogs": {"AutoEnable": True}},
        "MalwareProtection": {
            "ScanEc2InstanceWithFindings": {"EbsVolumes": {"AutoEnable": False}}
        },
    }
    auto_enable = True
    ret = await hub.exec.aws.guardduty.organization_configuration.update(
        ctx,
        resource_id=detector_id,
        auto_enable=auto_enable,
        data_sources=data_sources,
    )
    assert ret["result"], ret["comment"]
    assert f"{resource_type} '{detector_id}' already exists" in ret["comment"][0]


@pytest.mark.asyncio
@pytest.mark.localstack(
    False,
    "Skipping test in localstack as guardduty is not supported in localstack.",
)
async def test_organization_configuration_get(
    hub, ctx, aws_guardduty_detector, aws_guardduty_organization_master_account
):
    if hub.tool.utils.is_running_localstack(ctx):
        return
    resource_id = aws_guardduty_detector.get("resource_id")
    ret = await hub.exec.aws.guardduty.organization_configuration.get(
        ctx, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
